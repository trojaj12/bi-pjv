package theDrake.ui;

import theDrake.background.GameState;
import theDrake.ui.others.TileView;

public interface SelecterContext {

    public void selectableObjectSelected(SelectableItem selectableItem);

    public void updateGameState(GameState gameState);


}
