package theDrake.ui.colntrollers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;
import theDrake.background.*;
import theDrake.background.GameState;
import theDrake.ui.GameStateDependent;
import theDrake.ui.SelectableItem;
import theDrake.ui.SelecterContext;
import theDrake.ui.others.*;
import theDrake.ui.others.Stack;

import java.io.IOException;
import java.net.URL;
import java.util.*;

import static theDrake.ui.TheDrakeApp.createSampleGameState;

public class GameController implements Initializable, SelecterContext {

    @FXML
    private BoardView boardView;
    @FXML
    private Stack stackViewBlue;
    @FXML
    private Stack stackViewOrange;
    @FXML
    private CapturedView capturedViewOrange;
    @FXML
    private CapturedView capturedViewBlus;

    private GameState gameState;

    private ValidMoves validMoves;

    private SelectableItem selectedActiveItem;


    private List<theDrake.ui.GameStateDependent> GameStateDependent = new ArrayList<>();

    public void initialize() {

        gameState = createGameState(4, 3);
//        gameState = createSampleGameState();
        validMoves = new ValidMoves(gameState);

        initBoarView();
        initStack(stackViewBlue, PlayingSide.BLUE);
        initStack(stackViewOrange, PlayingSide.ORANGE);


        initCapturedView(capturedViewOrange, PlayingSide.ORANGE);
        initCapturedView(capturedViewBlus, PlayingSide.BLUE);
        updateGameStateDependents();
    }

    private void initCapturedView(CapturedView capturedViewOrange, PlayingSide side) {
        GameStateDependent.add(capturedViewOrange);
        capturedViewOrange.gameStateUpdate(gameState);
        capturedViewOrange.setPlayingSide(side);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        initialize();
    }

    private void initStack(Stack stackView, PlayingSide blue) {
        GameStateDependent.add(stackView);
        stackView.setPlayingSide(blue);
        stackView.initialize();
        stackView.setSelecterContext(this);
        stackView.setBoardView(boardView);
    }

    private void initBoarView() {
        GameStateDependent.add(boardView);
        boardView.setSelecterContext(this);
    }

    private GameState createGameState(int dim, int maxMauntin) {
        Board board = new Board(dim);
        PositionFactory positionFactory = board.positionFactory();
        Random random = new Random();
        for (int i = 0; i < random.nextInt(maxMauntin + 1); ++i) {
            board = board.withTiles(new Board.TileAt(positionFactory.pos(random.nextInt(dim), random.nextInt(dim)), BoardTile.MOUNTAIN));
        }
        return new StandardDrakeSetup().startState(board);
    }

    @Override
    public void updateGameState(GameState gameState) {
        this.gameState = gameState;
        updateGameStateDependents();
    }

    private void updateGameStateDependents() {
        testEndGame();
        for (GameStateDependent gameStateDependent : GameStateDependent) {
            gameStateDependent.gameStateUpdate(this.gameState);
        }
    }

    @Override
    public void selectableObjectSelected(SelectableItem selectableItem) {
        if (selectedActiveItem != null && selectableItem != selectedActiveItem)
            selectedActiveItem.unSelect();
        selectedActiveItem = selectableItem;
    }

    private void testEndGame() {
        if (gameState.result().equals(GameResult.VICTORY)) {

            ButtonType again = new ButtonType("znovu", ButtonBar.ButtonData.OK_DONE);
            ButtonType menu = new ButtonType("menu", ButtonBar.ButtonData.NO);
            Alert alert;
            if (gameState.sideOnTurn().equals(PlayingSide.BLUE)) {
                alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setTitle("Výhra");
                alert.setHeaderText("Oranžový vyhrál");
                alert.setContentText("Chcete hrát dál?");
            } else {
                alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setTitle("Výhra");
                alert.setHeaderText("Modrý vyhrál");
                alert.setContentText("Chcete hrát dál?");
            }
            Optional<ButtonType> result = alert.showAndWait();

            if (result.isPresent() && result.get() == ButtonType.OK)
                goAgain();
            else
                goMenu();
        }
    }

    private void goMenu() {
        try {
            Stage stage = (Stage) this.boardView.getScene().getWindow();

            FXMLLoader loader = new FXMLLoader(getClass().getResource("/theDrake/ui/fxml/MainMenu.fxml"));
            Parent root = null;

            root = loader.load();


            Scene scene = new Scene(root, 700, 400);
            scene.getStylesheets().add(getClass().getResource("/theDrake/ui/css/MainMenu.css").toExternalForm());
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void goAgain() {
        initialize();
    }

}
