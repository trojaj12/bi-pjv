package theDrake.ui.colntrollers;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import theDrake.background.GameState;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class MainMenuController {

    public void stopAction(ActionEvent ae) {
        Platform.exit();
    }

    public void twoPlayersGameAction(ActionEvent ae) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/theDrake/ui/fxml/Game.fxml"));

//        GameController controller = (GameController) loader.getController();
//        controller.initialize();

        Stage stage = (Stage) ((Node) ae.getSource()).getScene().getWindow();
        Parent root = loader.load();
        Scene scene = new Scene(root, 1000,800);
        stage.setScene(scene);
        scene.getStylesheets().add(getClass().getResource("/theDrake/ui/css/MainMenu.css").toExternalForm());
        stage.show();
    }

}
