package theDrake.ui.others;

import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import theDrake.background.GameState;
import theDrake.background.PlayingSide;
import theDrake.background.Troop;
import theDrake.ui.GameStateDependent;

import java.util.Collections;
import java.util.List;

public class CapturedView extends HBox implements GameStateDependent {
    private GameState gameState;
    private PlayingSide playingSide;
    private List<TroopView> list;

    public void setPlayingSide(PlayingSide playingSide) {
        this.playingSide = playingSide;
    }

    @Override
    public void gameStateUpdate(GameState gameState) {
        this.gameState = gameState;
        getChildren().clear();
        for (Troop troop : gameState.army(playingSide).captured()) {
            TroopView troopView = new TroopView(troop,playingSide == PlayingSide.BLUE ? PlayingSide.ORANGE : PlayingSide.BLUE,true);
            getChildren().add(0,troopView);
        }
    }
}
