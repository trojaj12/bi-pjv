package theDrake.ui.others;

import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import theDrake.background.PlayingSide;
import theDrake.background.Troop;
import theDrake.background.TroopFace;

public class TroopView extends Pane {
    private final TileBackgrounds backgrounds = new TileBackgrounds();
    private final Border activeBoarder = new Border(new BorderStroke(Color.RED, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, new BorderWidths(3)));

    public TroopView(Troop troop, PlayingSide playingSide, Boolean captured) {
            setPrefSize(100, 100);
        setBackground(backgrounds.getTroop(troop, playingSide, TroopFace.AVERS));
    }

    public void select()
    {
        setBorder(activeBoarder);
    }
    public void unselect()
    {
        setBorder(null);
    }



}
