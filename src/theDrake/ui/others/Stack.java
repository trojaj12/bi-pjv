package theDrake.ui.others;

import javafx.geometry.Insets;
import javafx.print.PageLayout;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import theDrake.background.GameState;
import theDrake.background.PlayingSide;
import theDrake.ui.GameStateDependent;
import theDrake.ui.SelectableItem;
import theDrake.ui.SelecterContext;

public class Stack extends VBox implements GameStateDependent, SelectableItem {

    private GameState gameState;
    private PlayingSide playingSide;
    private java.util.Stack<TroopView> stack;
    private SelecterContext selecterContext;
    private ValidMoves validMoves;

    public void setBoardView(BoardView boardView) {
        this.boardView = boardView;
    }

    private BoardView boardView;

    public void initialize() {
        setOnMouseClicked(e -> OnClick());
    }

    private void OnClick() {
        if (!stack.empty() && gameState.sideOnTurn().equals(playingSide))
            select();
    }

    private void select() {
        selecterContext.selectableObjectSelected(this);
        stack.peek().select();
        boardView.showMoves(validMoves.movesFromStack());
    }

    @Override
    public void setSelecterContext(SelecterContext selecterContext) {
        this.selecterContext = selecterContext;
    }

    public void unSelect() {
        if(stack!= null && !stack.empty())
            stack.peek().unselect();
    }

    @Override
    public void gameStateUpdate(GameState gameState) {
        this.gameState = gameState;
        unSelect();
        validMoves = new ValidMoves(gameState);
        stack = new java.util.Stack<>();
        getChildren().clear();

        for (int i = gameState.army(playingSide).stack().size() - 1; i >= 0; --i) {
            TroopView troopView = new TroopView(gameState.army(playingSide).stack().get(i), playingSide, false);
            stack.push(troopView);
            getChildren().add(troopView);
        }

    }

    public void setPlayingSide(PlayingSide playingSide) {
        this.playingSide = playingSide;
    }
}
