package theDrake.ui.others;

import java.util.List;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.layout.GridPane;
import theDrake.background.*;
import theDrake.ui.GameStateDependent;
import theDrake.ui.SelectableItem;
import theDrake.ui.SelecterContext;
import theDrake.ui.TileViewContext;
import theDrake.ui.others.ValidMoves;

public class BoardView extends GridPane implements TileViewContext, GameStateDependent, SelectableItem {

    private GameState gameState;

    private ValidMoves validMoves;

    private TileView selected;

    Boolean initialized = false;

    private SelecterContext selecterContext;

    private void initialize() {
        initialized = true;
        PositionFactory positionFactory = gameState.board().positionFactory();
        for (int y = 0; y < positionFactory.dimension(); ++y) {
            for (int x = 0; x < positionFactory.dimension(); ++x) {
                BoardPos boardPos = positionFactory.pos(x, positionFactory.dimension() - y - 1);
                add(new TileView(boardPos, gameState.tileAt(boardPos), this), x, y);
            }
        }

        setHgap(5);
        setVgap(5);
        setPadding(new Insets(15));
        setAlignment(Pos.CENTER);
    }

    @Override
    public void tileViewSelected(TileView tileView) {
        if (tileView != null)
            selecterContext.selectableObjectSelected(this);
        if (selected != null && selected != tileView)
            selected.unselect();

        selected = tileView;

        clearMoves();
        if(tileView != null)
            showMoves(validMoves.boardMoves(tileView.position()));
    }

    @Override
    public void executeMove(Move move) {
        if(selected != null)
            selected.unselect();
        selected = null;
        clearMoves();
        gameState = move.execute(gameState);
        validMoves = new ValidMoves(gameState);
        updateTiles();
        selecterContext.updateGameState(gameState);
    }

    private void updateTiles() {
        for (Node node : getChildren()) {
            TileView tileView = (TileView) node;
            tileView.setTile(gameState.tileAt(tileView.position()));
            tileView.update();
        }
    }

    private void clearMoves() {
        for (Node node : getChildren()) {
            TileView tileView = (TileView) node;
            tileView.clearMove();
        }
    }

    public void showMoves(List<Move> moveList) {
        for (Move move : moveList)
        {
            tileViewAt(move.target()).setMove(move);
        }
    }

    private TileView tileViewAt(BoardPos target) {
        int index = (3 - target.j()) * 4 + target.i();
        return (TileView) getChildren().get(index);
    }

    @Override
    public void gameStateUpdate(GameState gameState) {
        this.gameState = gameState;
        this.validMoves = new ValidMoves(gameState);
        if (!initialized)
            initialize();
        if (selected != null)
            selected.unselect();
        selected = null;
        clearMoves();
        updateTiles();
    }

    @Override
    public void setSelecterContext(SelecterContext selecterContext) {
        this.selecterContext = selecterContext;
    }

    @Override
    public void unSelect() {
        tileViewSelected(null);
    }
}
