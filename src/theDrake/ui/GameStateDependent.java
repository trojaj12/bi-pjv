package theDrake.ui;

import theDrake.background.GameState;

public interface GameStateDependent {

    void gameStateUpdate(GameState gameState);

}
