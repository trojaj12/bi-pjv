package theDrake.ui;

import theDrake.background.Move;
import theDrake.ui.others.TileView;

public interface TileViewContext {

    void tileViewSelected(TileView tileView);

    void executeMove(Move move);

}
