package theDrake.ui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import theDrake.ui.colntrollers.MainMenuController;


public class VMainMenu extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/theDrake/ui/fxml/MainMenu.fxml"));
        Parent root = loader.load();
        MainMenuController mainMenuController = loader.getController();

        Scene scene = new Scene(root, 700, 400);
        scene.getStylesheets().add(getClass().getResource("/theDrake/ui/css/MainMenu.css").toExternalForm());

        primaryStage.setTitle("The Drake");
        primaryStage.setScene(scene);
        primaryStage.setOnCloseRequest(eh -> mainMenuController.stopAction(null));
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

}
