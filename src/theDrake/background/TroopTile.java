package theDrake.background;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Implements Tile interface
 */
public class TroopTile implements Tile, JSONSerializable {

    /**
     * Troop standing on the Tile
     */
    private final Troop troop;
    /**
     * Side Troop is on
     */
    private final PlayingSide side;
    /**
     * Face Tropp is on
     */
    private final TroopFace face;

    /**
     * Constructs Troop
     *
     * @param troop troop
     * @param side  playing side
     * @param face  board face
     */
    public TroopTile(Troop troop, PlayingSide side, TroopFace face) {
        this.troop = troop;
        this.side = side;
        this.face = face;
    }

    /**
     * Getter
     *
     * @return side
     */
    public PlayingSide side() {
        return side;
    }

    /**
     * Getter
     *
     * @return fase
     */
    public TroopFace face() {
        return face;
    }

    /**
     * Getter
     *
     * @return troop
     */
    public Troop troop() {
        return troop;
    }

    /**
     * @return true, if Tile is free and can be stepped on
     */
    @Override
    public boolean canStepOn() {
        return false;
    }

    /**
     * @return true, if Tile contains SMTHing
     */
    @Override
    public boolean hasTroop() {
        return true;
    }

    @Override
    public List<Move> movesFrom(BoardPos pos, GameState state) {
        List<Move> result = new ArrayList<>();
        for (TroopAction action : troop.actions(face))
            result.addAll(action.movesFrom(pos, side, state));
        return result;
    }

    /**
     * Vytvoří novou dlaždici, s jednotkou otočenou na opačnou stranu
     * (z rubu na líc nebo z líce na rub)
     *
     * @return new TroopSide
     */
    public TroopTile flipped() {
        return new TroopTile(troop, side, (face == TroopFace.AVERS) ? TroopFace.REVERS : TroopFace.AVERS);
    }

    @Override
    public void toJSON(PrintWriter writer) {
        writer.print("{");

        writer.print("\"troop\":");
        troop.toJSON(writer);
        writer.print(",");

        writer.print("\"side\":");
        side.toJSON(writer);
        writer.print(",");

        writer.print("\"face\":");
        face.toJSON(writer);


        writer.print("}");
    }
}
