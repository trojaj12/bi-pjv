package theDrake.background;

import java.io.PrintWriter;
import java.util.*;
import java.util.stream.Collectors;

public class BoardTroops implements JSONSerializable {
    private final PlayingSide playingSide;
    private final Map<BoardPos, TroopTile> troopMap;
    private final TilePos leaderPosition;
    private final int guards;

    public BoardTroops(PlayingSide playingSide) {
        this.playingSide = playingSide;
        troopMap = Collections.emptyMap();
        this.leaderPosition = TilePos.OFF_BOARD;
        this.guards = 0;
    }

    public BoardTroops(PlayingSide playingSide, Map<BoardPos, TroopTile> troopMap, TilePos leaderPosition, int guards) {
        this.playingSide = playingSide;
        this.troopMap = troopMap;
        this.leaderPosition = leaderPosition;
        this.guards = guards;
    }

    public Optional<TroopTile> at(TilePos pos) {
        return (troopMap.containsKey(pos))
                ? Optional.of(troopMap.get(pos))
                : Optional.empty();
    }

    public PlayingSide playingSide() {
        return playingSide;
    }

    public TilePos leaderPosition() {
        return leaderPosition;
    }

    public int guards() {
        return guards;
    }

    public boolean isLeaderPlaced() {
        return at(leaderPosition).isPresent();
    }

    public boolean isPlacingGuards() {
        return guards < 2 && isLeaderPlaced();
    }

    public Set<BoardPos> troopPositions() {
        return troopMap.keySet();
    }

    public BoardTroops placeTroop(Troop troop, BoardPos target) {
        if (troopMap.containsKey(target))
            throw new IllegalArgumentException("Dlazdice jiz obsahuje neco");

        Map<BoardPos, TroopTile> newTroops = new HashMap<>(troopMap);
        newTroops.put(target, new TroopTile(troop, this.playingSide, TroopFace.AVERS));

        return (!isLeaderPlaced())
                ? new BoardTroops(this.playingSide, newTroops, target, this.guards) :
                isPlacingGuards()
                        ? new BoardTroops(this.playingSide, newTroops, this.leaderPosition, this.guards + 1)
                        : new BoardTroops(this.playingSide, newTroops, this.leaderPosition, this.guards);
    }

    public BoardTroops troopStep(BoardPos origin, BoardPos target) {
        if (!isLeaderPlaced() || isPlacingGuards())
            throw new IllegalStateException();
        if (!troopMap.containsKey(origin) || troopMap.containsKey(target))
            throw new IllegalArgumentException();

        Map<BoardPos, TroopTile> newTroops = new HashMap<>(troopMap);
        TroopTile tile = newTroops.remove(origin);
        newTroops.put(target, tile.flipped());
        return origin.equalsTo(leaderPosition.i(), leaderPosition().j())
                ? new BoardTroops(playingSide(), newTroops, target, guards)
                : new BoardTroops(playingSide(), newTroops, leaderPosition, guards);
    }

    public BoardTroops troopFlip(BoardPos origin) {
        if (!isLeaderPlaced()) {
            throw new IllegalStateException(
                    "Cannot move troops before the leader is placed.");
        }

        if (isPlacingGuards()) {
            throw new IllegalStateException(
                    "Cannot move troops before guards are placed.");
        }

        if (!at(origin).isPresent())
            throw new IllegalArgumentException();

        Map<BoardPos, TroopTile> newTroops = new HashMap<>(troopMap);
        TroopTile tile = newTroops.remove(origin);
        newTroops.put(origin, tile.flipped());

        return new BoardTroops(playingSide(), newTroops, leaderPosition, guards);
    }

    public BoardTroops removeTroop(BoardPos target) {
        if (!isLeaderPlaced() || isPlacingGuards())
            throw new IllegalStateException();
        if (!at(target).isPresent())
            throw new IllegalArgumentException();

        Map<BoardPos, TroopTile> newTroops = new HashMap<>(troopMap);
        newTroops.remove(target);

        return target.equals(leaderPosition)
                ? new BoardTroops(playingSide(), newTroops, TilePos.OFF_BOARD, guards)
                : new BoardTroops(playingSide(), newTroops, leaderPosition, guards);
    }

    @Override
    public void toJSON(PrintWriter writer) {
        writer.print("{");

        writer.print("\"side\":");
        playingSide.toJSON(writer);
        writer.print(",");

        writer.print("\"leaderPosition\":");
        leaderPosition.toJSON(writer);
        writer.print(",");

        writer.print("\"guards\":" + guards());
        writer.print(",");


        writer.print("\"troopMap\":");
        writer.print("{");

        List<BoardPos> keys = troopMap.keySet().stream().sorted(Comparator.comparing(BoardPos::toString)).collect(Collectors.toList());
        if(keys.size() != 0) {
            BoardPos last = keys.get(keys.size() - 1);
            keys.remove(keys.size() - 1);
            for (BoardPos key : keys) {
                key.toJSON(writer);
                writer.print(":");
                troopMap.get(key).toJSON(writer);
                writer.print(",");
            }
            last.toJSON(writer);
            writer.print(":");
            troopMap.get(last).toJSON(writer);
        }
        writer.print("}");

        writer.print("}");
    }
}
