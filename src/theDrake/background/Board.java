package theDrake.background;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.StringJoiner;

public class Board implements JSONSerializable {

	private final BoardTile[][] arr;


	/// Konstruktor. Vytvoří čtvercovou hrací desku zadaného rozměru, kde všechny dlaždice jsou prázdné, tedy BoardTile.EMPTY
	public Board(int dimension) {
		arr = new BoardTile[dimension][dimension];
		for ( Tile[] row : arr )
			Arrays.fill(row, BoardTile.EMPTY);
	}

	/// Rozměr hrací desky
	public int dimension() {
		return arr.length;
	}

	/// Vrací dlaždici na zvolené pozici.
	public BoardTile at(TilePos pos) {
		return arr[pos.i()][pos.j()];
	}
	public BoardTile atCoor(int i, int j) {
		return arr[i][j];
	}

	/// Vytváří novou hrací desku s novými dlaždicemi. Všechny ostatní dlaždice zůstávají stejné
	public Board withTiles(TileAt ...ats) {
		Board ret = new Board(this.dimension());
		for (int i = 0; i < dimension(); ++i)
			ret.arr[i] = arr[i].clone();
		for(TileAt t : ats)
			ret.arr[t.pos.i()][t.pos.j()] = t.tile;
		return ret;
	}

	/// Vytvoří instanci PositionFactory pro výrobu pozic na tomto hracím plánu
	public PositionFactory positionFactory() {
		return new PositionFactory(dimension());
	}

	@Override
	public void toJSON(PrintWriter writer) {
		writer.print("{");
		writer.print("\"dimension\":" + this.dimension());
		writer.print(",");

		writer.print("\"tiles\":");
		StringJoiner joiner = new StringJoiner(",", "[", "]");

		for (int i = 0; i < arr.length; i++)
			for (int j = 0; j < arr[i].length; j++)
				joiner.add( '"' + arr[j][i].toString() + '"');
		writer.print(joiner.toString());

		writer.print("}");
	}


	public static class TileAt {
		public final BoardPos pos;
		public final BoardTile tile;
		
		public TileAt(BoardPos pos, BoardTile tile) {
			this.pos = pos;
			this.tile = tile;
		}
	}
}

