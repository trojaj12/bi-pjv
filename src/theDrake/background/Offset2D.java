package theDrake.background;

/**
 * Reprezentuje diskrétní posunutí ve 2D prostoru
 * má souřadnice X,Y
 */
public class Offset2D {

    /**
     * Value of X coordinate
     */
    public final int x;
    /**
     * Value of Y coordinate
     */
    public final int y;

    /**
     * Constructor
     * Reprezentuje diskrétní posunutí ve 2D prostoru
     * @param x Value of X coordinate
     * @param y Value of Y coordinate
     */
    public Offset2D(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Check if offset is equal to other offset
     *
     * @param x Value of X coordinate
     * @param y Value of Y coordinate
     * @return True if its same, otherwise False
     */
    public boolean equalsTo(int x, int y) {
        return (this.x == x && this.y == y);
    }

    /**
     * Creates new offset, where coordinate Y has opposite value
     *
     * @return offset, where coordinate Y has opposite value
     */
    public Offset2D yFlipped() {
        return new Offset2D(this.x, -y);
    }

}
