package theDrake.background;

import java.io.PrintWriter;

/**
 * Represent TroopFace
 * Reprezentuje lícovou/rubovou stranu
 */
public enum TroopFace implements JSONSerializable {
    AVERS, REVERS;

    @Override
    public void toJSON(PrintWriter writer) {
        writer.print("\"" + this.toString() + "\"");
    }
}
