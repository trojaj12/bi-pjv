package theDrake.background;

import java.io.PrintWriter;

public enum GameResult implements JSONSerializable {
	VICTORY, DRAW, IN_PLAY;

	@Override
	public void toJSON(PrintWriter writer) {
		writer.print('"' + this.toString() + '"');
	}
}
