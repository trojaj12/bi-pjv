package theDrake.background;

import java.io.PrintWriter;
import java.util.List;

/**
 * Represent a Troop
 * představovat bojovou jednotku v naší hře
 */
public class Troop implements JSONSerializable {
    /**
     * Jméno jenotky
     */
    private final String name;
    /**
     * Pozice na lícové straně
     */
    private Offset2D aversOffset;
    /**
     * Pozice na rubové straně
     */
    private Offset2D reversOffset;

    private final List<TroopAction> aversActions;
    private final List<TroopAction> reversActions;

    /**
     * Constructor
     * představovat bojovou jednotku v naší hře
     * @param name         Name of the troop
     * @param aversOffset  Position on the avers board
     * @param reversOffset Position on the revers board
     */
    public Troop(String name, Offset2D aversOffset, Offset2D reversOffset, List<TroopAction> aversActions, List<TroopAction> reversActions) {
        this.name = name;
        this.aversOffset = aversOffset;
        this.reversOffset = reversOffset;
        this.aversActions = aversActions;
        this.reversActions = reversActions;
    }
    /**
     * Constructor
     * Both positions are same
     * představovat bojovou jednotku v naší hře
     * @param name   Name of the troop
     * @param offset Position on the board
     */
    public Troop(String name, Offset2D offset, List<TroopAction> aversActions, List<TroopAction> reversActions) {
        this.name = name;
        this.aversOffset = this.reversOffset = offset;
        this.aversActions = aversActions;
        this.reversActions = reversActions;
    }
    /**
     * Constructor
     * Both positions are at [1,1]
     * představovat bojovou jednotku v naší hře
     * @param name Name of the troop
     */
    public Troop(String name, List<TroopAction> aversActions, List<TroopAction> reversActions) {
        this.name = name;
        this.aversActions = aversActions;
        this.reversActions = reversActions;
        this.aversOffset = this.reversOffset = new Offset2D(1, 1);
    }

    /**
     * Vrací jméno jednotky
     *
     * @return name of the Troop
     */
    public String name() {
        return name;
    }

    /**
     * Vrací pozici jenotky
     *
     * @param face Face of the card
     * @return position of the Troop
     */
    public Offset2D pivot(TroopFace face) {
        return (face == TroopFace.AVERS)
                ? this.aversOffset
                : this.reversOffset;
    }

    public List<TroopAction> actions(TroopFace face){
        return (face.equals(TroopFace.AVERS))
                ?aversActions
                :reversActions;
    }

    @Override
    public void toJSON(PrintWriter writer) {
        writer.print('"' + name() +'"');
    }
}
