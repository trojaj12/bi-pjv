package theDrake.background;

import java.io.PrintWriter;

/**
 * Represents players color
 */
public enum PlayingSide implements JSONSerializable {
    ORANGE,BLUE;

    @Override
    public void toJSON(PrintWriter writer) {
        writer.print("\"" + this.toString() + "\"");
    }
}
