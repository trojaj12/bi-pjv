module TheDrake {
    requires javafx.controls;
    requires javafx.fxml;

    opens assets;
    opens theDrake.background;
    opens theDrake.ui;
    opens theDrake.ui.colntrollers;
    opens theDrake.ui.css;
    opens theDrake.ui.fxml;
    opens theDrake.ui.others;
//    opens theDrake.ui.others.BoardView;
//    opens theDrake.ui.others.stack;
}